#version 450

/* const vec2 positions[3] = vec2[3]( */
/*     vec2(0.0, 0.5), */
/*     vec2(-0.5, -0.5), */
/*     vec2(0.5, -0.5) */
/* ); */

/* const vec2 positions[6] = vec2[6]( */
/*     vec2(-1., 1.), */
/*     vec2(-1., -1.), */
/*     vec2(1., 1.), */
/*     vec2(1., -1.), */
/*     vec2(1., 1.), */
/*     vec2(-1., -1.) */
/* ); */

// This is clockwise
/* const vec2 positions[4] = vec2[4]( */
/*     vec2(1., -1.), */
/*     vec2(-1., -1.), */
/*     vec2(-1., 1.), */
/*     vec2(1., 1.) */
/* ); */

layout(location = 0) in vec3 position;

layout(location = 0) out vec3 coord;

void main() {
    coord = position;

    gl_Position = vec4(position, 1.0);
}
