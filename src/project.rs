use serde_derive::{Deserialize, Serialize};

use log::info;

use std::{
    collections::HashMap,
    fs::{self, create_dir, File, OpenOptions},
    io::{Read, Write},
    path::{Path, PathBuf},
};

/// Defines how the project should be saved.
#[derive(Debug, Clone)]
pub enum SavingMethod {
    /// Save current project to specified path. This method assumes the target directory
    /// doesn't exist yet.
    NewDirectory(String),

    /// Save current project to `ProjectInfo::path`. This method assumes the project directory has
    /// already been created.
    InPlace,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ResourceId(String);

impl ResourceId {
    pub fn id(&self) -> &str {
        &self.0
    }
}

impl From<String> for ResourceId {
    fn from(id: String) -> Self {
        Self(id)
    }
}

impl From<&String> for ResourceId {
    fn from(id: &String) -> Self {
        Self(id.clone())
    }
}

impl From<&str> for ResourceId {
    fn from(id: &str) -> Self {
        Self(id.to_string())
    }
}

#[derive(Debug)]
pub enum ResourceStorage {
    Temporary(File),
    OnDisk(PathBuf),
}

#[derive(Debug, PartialEq, Eq)]
pub enum ContentKind {
    Binary,
    PlainText,
}

#[derive(Debug, PartialEq, Eq)]
pub struct ResourceKind {
    content_kind: ContentKind,
    extension: &'static str,
}

impl ResourceKind {
    pub const FRAGMENT_SHADER: ResourceKind = ResourceKind {
        content_kind: ContentKind::PlainText,
        extension: "frag",
    };

    pub const IMAGE_PNG: ResourceKind = ResourceKind {
        content_kind: ContentKind::Binary,
        extension: "png",
    };
}

#[derive(Debug)]
pub struct Resource {
    id: ResourceId,
    storage: ResourceStorage,
    kind: ResourceKind,
    content: String,
}

/// **TODO:** Does it make sense to just not make `Resource`'s fields pub?
impl Resource {
    pub fn id(&self) -> &ResourceId {
        &self.id
    }

    pub fn storage(&self) -> &ResourceStorage {
        &self.storage
    }

    pub fn kind(&self) -> &ResourceKind {
        &self.kind
    }

    pub fn content(&self) -> &String {
        &self.content
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Fields {
    pub resolution: [u32; 2],
    /// Contain the name of the fragment shader which can be used to index through
    /// `Project::resources` to obtain the actual resource's path.
    pub fragment_shader: Option<String>,
}

impl Default for Fields {
    fn default() -> Self {
        Self {
            resolution: [800, 600],
            fragment_shader: None,
        }
    }
}

pub struct Info {
    /// Name of the project.
    pub name: String,
    /// Path to the project file (i.e. `/path/project_name.zei`).
    pub path: PathBuf,
}

pub struct Project {
    pub info: Info,
    pub fields: Fields,
    resources: HashMap<ResourceId, Resource>,
}

impl Project {
    /// Create an empty, default project.
    pub fn new(name: impl Into<String>, path: impl Into<PathBuf>) -> Self {
        let info = Info {
            name: name.into(),
            path: path.into(),
        };

        Self {
            info,
            fields: Default::default(),
            resources: HashMap::new(),
        }
    }

    /// Load project from a project file.
    ///
    /// **TODO:** The creation of resources' field should check that resource paths actually point to
    /// existing files of the right nature.
    pub fn from_path(path: impl Into<PathBuf> + Clone) -> Self {
        let fields = {
            let mut file = File::open(&path.clone().into()).expect("Couldn't open project file");
            let mut content = String::new();

            file.read_to_string(&mut content)
                .expect("Couldn't read project file");

            toml::from_str::<Fields>(&content).expect("Couldn't deserialize the project file")
        };

        let info = {
            let path = path.into();
            let name = path.file_stem().unwrap().to_str().unwrap().to_string();

            Info { name, path }
        };

        let resources = {
            let mut resources: HashMap<ResourceId, Resource> = HashMap::new();

            if let Some(fragment_shader) = &fields.fragment_shader {
                let path = Self::project_parent_path_of(&info.path)
                    .join(&fragment_shader)
                    .with_extension("frag");

                let id = ResourceId(fragment_shader.clone());

                let resource = {
                    let mut resource_file = File::open(&path)
                        .expect(&format!("Couldn't open fragment shader at: {:?}", &path));
                    let mut resource = String::new();

                    resource_file.read_to_string(&mut resource).unwrap();

                    Resource {
                        id: id.clone(),
                        storage: ResourceStorage::OnDisk(path),
                        kind: ResourceKind::FRAGMENT_SHADER,
                        content: resource,
                    }
                };

                resources.insert(id, resource);
            }

            resources
        };

        Self {
            info,
            fields,
            resources,
        }
    }

    pub fn resources(&self) -> &HashMap<ResourceId, Resource> {
        &self.resources
    }

    /// Copy source shader to a temporary location and add it to `Self::resources_path` as a
    /// `ResourcePath::Temp(_)`.
    ///
    /// Return an owned `ResourceId` of the imported resource.
    pub fn import_shader(&mut self, src_path: impl AsRef<Path>) -> ResourceId {
        // Remove the fragment shader from `self.resources_path` if one already exists there.
        //
        // NOTE: In the future, when resources begin to be more dynamic, it would be a good idea to
        // make the user explicitely request the removal of a newly unused resource via the ui so
        // that we don't have to keep track to the resources ourselves and can properly remove
        // unused ondisk resources.
        if let Some(fragment_shader) = &self.fields.fragment_shader {
            self.resources.remove(&ResourceId(fragment_shader.clone()));
        }

        let mut src_file = File::open(&src_path).expect("Couldn't read source shader file.");
        let temp_file = tempfile::tempfile().expect("Couldn't create temporary shader file.");

        let src_name = src_path
            .as_ref()
            .file_stem()
            .unwrap()
            .to_str()
            .unwrap()
            .to_owned();

        let id = ResourceId(src_name.clone());

        let resource_content = {
            let mut content = String::new();
            src_file.read_to_string(&mut content).unwrap();
            content
        };

        let resource = {
            Resource {
                id: id.clone(),
                storage: ResourceStorage::Temporary(temp_file),
                kind: ResourceKind::FRAGMENT_SHADER,
                content: resource_content,
            }
        };

        self.resources.insert(id.clone(), resource);

        self.fields.fragment_shader = Some(src_name.clone());

        id
    }

    /// Save the project with methods stated in `SavingMethod`.
    ///
    /// When `method` is `SavingMethod::NewDirectory(_)`, resources should be copied to the new
    /// repertory.
    ///
    /// For all `SavingMethod`s, every resources that are `ResourcePath::Temp(_)` will be copied
    /// from their temporary directory to the actual project directory.
    ///
    /// **TODO:** Make this function and the others return a Result and act based on that in
    /// `Application`.
    pub fn save(&mut self, method: &SavingMethod) {
        let (mut project_file, project_file_path) = {
            let mut project_file = OpenOptions::new();

            let project_file_path = match method {
                SavingMethod::NewDirectory(parent_project_path) => {
                    let project_dir_path = PathBuf::from(parent_project_path).join(&self.info.name);

                    if project_dir_path.exists() {
                        info!("Another folder with the same name as the project already exists!");
                        return;
                    }

                    create_dir(&project_dir_path).expect("Couldn't create project directory.");

                    project_file.create(true).write(true);

                    project_dir_path.join(&self.info.name).with_extension("zei")
                }
                SavingMethod::InPlace => {
                    project_file.write(true).truncate(true);

                    self.info.path.clone()
                }
            };

            (
                project_file
                    .open(&project_file_path)
                    .expect("Couldn't open project file."),
                project_file_path,
            )
        };

        let project_serialized =
            toml::to_string_pretty(&self.fields).expect("Couldn't serialize project struct.");

        project_file
            .write(project_serialized.as_bytes())
            .expect("Couldn't write project file.");

        for (id, resource) in self.resources.iter_mut() {
            match (method, resource) {
                (
                    _,
                    Resource {
                        content,
                        storage: ResourceStorage::Temporary(_),
                        kind: ResourceKind { extension, .. },
                        ..
                    },
                ) => {
                    // TODO: Update the storage once saved?
                    let mut dest = {
                        let dest_path = Self::project_parent_path_of(&project_file_path)
                            .join(id.id())
                            .with_extension(extension);
                        File::create(dest_path)
                            .expect(&format!("Couldn't import resource id:\"{}\"", id.id()))
                    };

                    dest.write(content.as_bytes()).unwrap();
                }
                (
                    SavingMethod::NewDirectory(_),
                    Resource {
                        storage: ResourceStorage::OnDisk(src_path),
                        kind: ResourceKind { extension, .. },
                        ..
                    },
                ) => {
                    let dest_path = Self::project_parent_path_of(&project_file_path)
                        .join(id.id())
                        .with_extension(extension);

                    fs::copy(&src_path, &dest_path).unwrap();
                }
                _ => (),
            }
        }
    }

    /// Obtain the folder's path in which the project's folder is located.
    fn project_parent_path(&self) -> PathBuf {
        if let Some(project_parent) = self.info.path.parent() {
            project_parent.to_owned()
        } else {
            PathBuf::new()
        }
    }

    /// Obtain the folder's path in which the specified folder is located.
    fn project_parent_path_of(path: &PathBuf) -> PathBuf {
        if let Some(project_parent) = path.parent() {
            project_parent.to_owned()
        } else {
            PathBuf::new()
        }
    }
}
