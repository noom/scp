use wgpu::{util::DeviceExt, *};

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    position: [f32; 3],
}

unsafe impl bytemuck::Pod for Vertex {}
unsafe impl bytemuck::Zeroable for Vertex {}

impl Vertex {
    pub fn desc<'a>() -> VertexBufferDescriptor<'a> {
        VertexBufferDescriptor {
            stride: std::mem::size_of::<Vertex>() as BufferAddress,
            step_mode: InputStepMode::Vertex,
            attributes: &[VertexAttributeDescriptor {
                offset: 0,
                shader_location: 0,
                format: VertexFormat::Float3,
            }],
        }
    }
}

pub const QUAD_VERTICES: &[Vertex] = &[
    Vertex {
        position: [-1., 1., 0.],
    },
    Vertex {
        position: [-1., -1., 0.],
    },
    Vertex {
        position: [1., 1., 0.],
    },
    Vertex {
        position: [1., -1., 0.],
    },
];
pub const QUAD_INDICES: &[u16] = &[0, 1, 2, 2, 1, 3];

pub fn make_quad_vertex_buffer(device: &Device) -> Buffer {
    device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Vertex Buffer"),
        contents: bytemuck::cast_slice(QUAD_VERTICES),
        usage: wgpu::BufferUsage::VERTEX,
    })
}

pub fn make_quad_index_buffer(device: &Device) -> Buffer {
    device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Index Buffer"),
        contents: bytemuck::cast_slice(QUAD_INDICES),
        usage: wgpu::BufferUsage::INDEX,
    })
}
